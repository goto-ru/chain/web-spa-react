import React from 'react';


export default class extends React.Component {
  handleChange = ({ target }) => {
    const name = target.name;

    this.setState({ [name]: target.value });
  }


  /* FIXME: TypeError: _get(...) is undefined on call
     due to binding @ `onSubmit`, seemingly
  handleSubmit = event => {
    event.preventDefault();
  }
  */
}


