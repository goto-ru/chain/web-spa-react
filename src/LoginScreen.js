import React from 'react';
import { connect } from 'react-redux'
import { TxCreateWallet } from '@goto-ru/chain-jsclient';
import * as R from './Redux';
import Form from './Form';
const Exonum = require('exonum-client'); // FIXME: should be ES6 import


// FIXME: too verbose, needs FRP
class RegistrationForm extends Form {
  state = {
    name : '',
    age: 0,
    email: '',
    city: '',
    school: '',
  };

  handleSubmit = event => {
    const keys = Exonum.keyPair();

    const {age, ...x} = this.state;
    const walletData = {
        pub_key: keys.publicKey,
        age: String(age),
        ...x,
    };

    const tx = TxCreateWallet.json(walletData, keys.secretKey);

    // FIXME: error handling
    fetch('http://localhost:8888/api/services/cryptocurrency/v1/wallets/transaction', {method: 'POST', body: tx})
      .then (r => !r.ok ? Error(r.statusText) : r)
      .then(result => { this.props.dispatch(R.importPrivateKey(keys.secretKey)); });

    event.preventDefault();
    // super.handleSubmit(event);
  }

  // FIXME: factor out pattern
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />
        </label>
        <label>
          Age:
          <input type="number" name="age" value={this.state.age} onChange={this.handleChange} />
        </label>
        <label>
          Email:
          <input type="email" name="email" value={this.state.email} onChange={this.handleChange} />
        </label>
        <label>
          City:
          <input type="text" name="city" value={this.state.city} onChange={this.handleChange} />
        </label>
        <label>
          School:
          <input type="text" name="school" value={this.state.school} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

RegistrationForm = connect()(RegistrationForm);

let PrivateKey = ({ secretKey }) => <p>{secretKey}</p>;
PrivateKey = connect(({ keys: { secretKey }}) => ({ secretKey }))(PrivateKey);

class LoginForm extends Form {
  state = {
    key: '',
  };

  handleSubmit = event => {
    // FIXME: only show on successful CR
    this.props.dispatch(R.importPrivateKey(this.state.key));

    event.preventDefault();
    // super.handleSubmit(event);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Ключ:
          <input type="text" name="key" value={this.state.key} onChange={this.handleChange} />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

LoginForm = connect()(LoginForm);

export default () =>
  <section>
    <section>
      <header><h2>Регистрация</h2></header>
      <RegistrationForm />
      <PrivateKey />
    </section>
    <section>
      <header><h2>Вход</h2></header>
      <LoginForm />
    </section>
  </section>;
