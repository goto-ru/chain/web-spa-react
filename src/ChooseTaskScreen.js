import React from 'react';
import { connect } from 'react-redux'
import * as R from './Redux';
import Form from './Form';
import { TxAssignTask, TxSubmitSolution } from '@goto-ru/chain-jsclient';

/*
  Страница с краткими описаниями конкурсов + статус ваших заявок
  Здесь юзер знакомится с доступными грантами, выбирает желаемый, подписывает котракт на его выполнения после этого ему становится доступным подробная информация.
*/
/*
  1. Fetch task list
  2. "Me want this one" => Tx
  3. Fetch additional info about (2)?
*/

const Row = ({ name, onClick }) =>
  <div>
    <span>{name}</span>
    <button onClick={onClick}>Хочу!</button>
  </div>;

const mkTx = ({ publicKey, secretKey }, taskId) => TxAssignTask.json({ task_hash: taskId, author: publicKey }, secretKey);


class SubmitSolutionForm extends Form {
  state = {
    url: '',
  };

  handleSubmit = event => {
    const tx = TxSubmitSolution.json({ solution_hash: this.props.id, url: this.state.url }, this.props.keys.secretKey);
    this.props.dispatch(R.submitSolution(tx));

    event.preventDefault();
    // super.handleSubmit(event);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" name="url" value={this.state.key} onChange={this.handleChange} placeholder="https://git.example.com/user/repo.git" />
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

SubmitSolutionForm = connect(({ keys }) => ({ keys }))(SubmitSolutionForm);


class TaskList extends React.Component {
  componentWillMount() {
    this.props.dispatch(R.loadTasks());
  }

  render() {
    // FIXME: boilerplate, needs @ syntax
    return (
      <div>
        {Object.entries(this.props.tasks).map(([k, { name }]) =>
          <Row key={k} name={name}
            onClick={() => this.props.dispatch(R.assignTask(k, mkTx(this.props.keys, k)))}
          />)
        }
      </div>
    );
  }
}

TaskList = connect(({ tasks, keys }) => ({ tasks, keys }))(TaskList);


let AssignedTaskList = ({ task_solution, tasks }) => {
  const assignedTasks = Object.entries(task_solution).map(([task_id, solution_id]) => ({ ...tasks[task_id], id: solution_id }));

  return (
    <div>
      {assignedTasks.map(({ name, desc, id }) =>
        <div key={name}>
          <h3>{name}</h3>
          <p>{desc}</p>
          <section>
            <h4>Отправить решение</h4>
            <SubmitSolutionForm id={id} />
          </section>
        </div>
      )}
    </div>
  );
};

AssignedTaskList = connect(({ task_solution, tasks }) => ({ task_solution, tasks }))(AssignedTaskList);


const SolutionSubmissionStatus = connect(({ last_submit_ok }) => ({ last_submit_ok }))(({ last_submit_ok }) =>
  <p>{last_submit_ok ? "Решение отправлено." : "Не зашло."}</p>
);


export default () =>
  <div>
    <TaskList />
    <AssignedTaskList />
    <SolutionSubmissionStatus />
  </div>;
