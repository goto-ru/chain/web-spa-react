import React from 'react';
import { Provider } from 'react-redux'
import { compose, createStore, applyMiddleware } from 'redux'
import { middleware as reduxPackMiddleware } from 'redux-pack';
import {persistStore, autoRehydrate} from 'redux-persist';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import * as R from './Redux';
import PreambuleScreen from './PreambuleScreen.js';
import LoginScreen from './LoginScreen.js';
import ChooseTaskScreen from './ChooseTaskScreen.js';


const store = createStore(
  R.reduce,
  undefined,
  compose(
    autoRehydrate(),
    applyMiddleware(reduxPackMiddleware)
  )
)
persistStore(store)


export default () =>
  <Provider store={store}>
    <Router>
      <div>
        <Route exact path="/" component={PreambuleScreen} />
        <Route path="/login" component={LoginScreen} />
        <Route path="/choose_task" component={ChooseTaskScreen} />
      </div>
    </Router>
  </Provider>
