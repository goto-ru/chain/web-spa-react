import { handle } from 'redux-pack';
const Exonum = require('exonum-client'); // FIXME: should be ES6 import


/// Actions
export const IMPORT_PRIVATE_KEY = 'IMPORT_PRIVATE_KEY';

export const LOAD_TASKS = 'LOAD_TASKS';

export const ASSIGN_TASK = 'ASSIGN_TASK';

export const SUBMIT_SOLUTION = 'SUBMIT_SOLUTION';

/// Action creators
export const importPrivateKey = (privateKey) => ({ type: IMPORT_PRIVATE_KEY, privateKey });

export const loadTasks = () => ({ type: LOAD_TASKS, promise: fetch('http://localhost:8888/api/services/cryptocurrency/v1/contracts/open').then(r => r.json()) });

export const assignTask = (task_id, tx) => ({ type: ASSIGN_TASK, promise: fetch('http://localhost:8888/api/services/cryptocurrency/v1/wallets/transaction', {method: 'POST', body: tx}).then(r => !r.ok ? Error(r.statusText) : r).then(r => r.json()).then(x => x['tx_hash']).then(solution_id => [task_id, solution_id]) });

export const submitSolution = tx => ({ type: SUBMIT_SOLUTION, promise: fetch('http://localhost:8888/api/services/cryptocurrency/v1/wallets/transaction', {method: 'POST', body: tx}).then(r => !r.ok ? Error(r.statusText) : r) });

/// Reducers
const initialState = { keys: {}, tasks: {}, task_solution: {}, last_submit_ok: undefined };

export const reduce = (state = initialState, action) =>
    action.type === IMPORT_PRIVATE_KEY ? { ...state, keys: Exonum.fromSecretKey(action.privateKey) } :
    action.type === LOAD_TASKS         ? handle(state, action, {
                                            success: s => ({ ...s, tasks: action.payload })
                                         }) :
    action.type === ASSIGN_TASK        ? handle(state, action, {
                                            success: s => {
                                                const [ task_id, solution_id ] = action.payload;
                                                return ({ ...s, task_solution: { ...s.task_solution, [task_id]: solution_id } });
                                            },
                                         }) :
    action.type === SUBMIT_SOLUTION    ? handle(state, action, {
                                            success: s => ({ ...s, last_submit_ok: true }),
                                            failure: s => ({ ...s, last_submit_ok: false }),
                                         }) :
                                         state
